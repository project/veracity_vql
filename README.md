# Veracity Integration

## INTRODUCTION
------------

The Veracity Integration module allows you to define a VQL queries via the Drupal Interface and execute those queries against a [Veracity LRS](https://lrs.io) to render charts.

## Issues and contributions
------------

Issues and development happens in the [Drupal.org issue queue](https://www.drupal.org/project/issues/veracity_vql).


REQUIREMENTS
------------

* [Veracity LRS](https://lrs.io)
* Veracity Access Key with Advance Query permissions

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

Configuration of Flag module involves creating one or more flags.

 1. Go to Admin >  Configuration > Web services  > Veracity

 2. Enter the Veracity URL, Access Key, and Access Key Secret

 3. Add a new VQL Chart block

 4. Enter VQL query in VQL textarea

 5. Click Save Block


