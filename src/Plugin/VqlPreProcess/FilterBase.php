<?php

namespace Drupal\veracity_vql\Plugin\VqlPreProcess;

use Drupal\Component\Utility\NestedArray;
use Drupal\veracity_vql\Plugin\VqlPreProcessBase;
use Drupal\veracity_vql\VqlHelper;

/**
 * Base implementation of a pre-process plugin to filter results.
 */
abstract class FilterBase extends VqlPreProcessBase {

  /**
   * Retrieve a nested, associative array containing filter values.
   *
   * @return array
   *   Filter values.
   */
  abstract public function getFilter(): array;

  /**
   * {@inheritdoc}
   */
  public function alterQuery(array &$query) {
    if (!isset($query['filter'])) {
      $query['filter'] = [];
    }

    $filter = VqlHelper::filterParser($this->getFilter());
    $query['filter'] = NestedArray::mergeDeep($query['filter'], $filter);
  }

}
