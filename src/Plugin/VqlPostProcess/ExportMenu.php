<?php

namespace Drupal\veracity_vql\Plugin\VqlPostProcess;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\veracity_vql\Plugin\VqlPostProcessBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds export options to the chart.
 *
 * Only charts rendered with amcharts support this.
 * (Excludes notice, list, and tables.)
 *
 * @see https://www.amcharts.com/docs/v4/concepts/exporting/
 *
 * @VqlPostProcess(
 *   id = "export_menu",
 *   label = "Export Menu",
 *   description = "Adds a menu for exporting chart data."
 * )
 */
class ExportMenu extends VqlPostProcessBase implements ContainerFactoryPluginInterface {

  /**
   * Current token replacement service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Supported chart formats by am4charts.
   *
   * @var array
   */
  protected $chartFormats = [
    'png' => 'PNG',
    'jpg' => 'JPG',
    'svg' => 'SVG',
    'pdf' => 'PDF',
    'json' => 'JSON',
    'csv' => 'CSV',
    'xlsx' => 'XLSX (Excel)',
    'html' => 'HTML',
    'pdfdata' => 'PDF (Data)',
    'print' => 'Print',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->token = $container->get('token');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'menu' => [
        'align' => 'right',
        'verticalAlign' => 'top',
      ],
      'filePrefix' => 'Export',
      'formatOptions' => [
        'json' => [
          'disabled' => TRUE,
        ],
        'html' => [
          'disabled' => TRUE,
        ],
        'pdfdata' => [
          'disabled' => TRUE,
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration() + $this->defaultConfiguration();

    $form['menu']['align'] = [
      '#type' => 'select',
      '#title' => $this->t('Horizontal Position'),
      '#options' => [
        'left' => $this->t('Left'),
        'middle' => $this->t('Middle'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $config['menu']['align'],
    ];

    $form['menu']['verticalAlign'] = [
      '#type' => 'select',
      '#title' => $this->t('Vertical Position'),
      '#options' => [
        'top' => $this->t('Top'),
        'middle' => $this->t('Middle'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $config['menu']['verticalAlign'],
    ];

    $form['filePrefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File Name'),
      '#default_value' => $config['filePrefix'],
    ];
    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [],
    ];

    $form['formatOptions'] = [
      '#type' => 'details',
      '#title' => $this->t('Formats'),
    ];
    foreach ($this->chartFormats as $format => $label) {
      $form['formatOptions'][$format]['disabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide @format', ['@format' => $label]),
        '#default_value' => $config['formatOptions'][$format]['disabled'] ?? FALSE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $format_options = array_filter($form_state->getValue('formatOptions', []), function ($format) {
      return $format['disabled'];
    });
    $form_state->setValue('formatOptions', $format_options);

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function processResult(array &$result) {
    foreach ($result as &$data) {
      if (!isset($data['chart'])) {
        continue;
      }

      if (!isset($data['chart']['exporting'])) {
        $data['chart']['exporting'] = [];
      }

      $data['chart']['exporting'] += $this->configuration;

      $filename = $this->token->replace($this->configuration['filePrefix']);
      if (!empty($filename)) {
        $data['chart']['exporting']['filePrefix'] = $filename;
      }
    }
  }

}
