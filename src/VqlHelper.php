<?php

namespace Drupal\veracity_vql;

/**
 * Helper class to format VQL query.
 */
class VqlHelper {

  /**
   * Parse the filter section of vql query.
   *
   * @param array $filter
   *   The filter part of VQL query.
   *
   * @return array
   *   The extracted dynamic filter keys.
   *   Like
   *     [
   *       filterKey => $filterPlaceholder,
   *       filterKey => $filterPlaceholder,
   *     ]
   */
  public static function filterParser(array $filter): array {
    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($filter));
    $result = [];

    foreach ($iterator as $value) {
      $keys = [];
      foreach (range(0, $iterator->getDepth()) as $depth) {
        $key = $iterator->getSubIterator($depth)->key();
        if (substr($key, 0, 1) === '$') {
          $value = [$key => $iterator->getSubIterator($depth)->current()];
          break;
        }
        $keys[] = $key;
      }

      if (empty($keys)) {
        $result += $value;
      }
      else {
        $result[implode('.', $keys)] = $value;
      }
    }

    return $result;
  }

}
