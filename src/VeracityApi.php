<?php

namespace Drupal\veracity_vql;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\veracity_vql\Event\VqlPostExecuteEvent;
use Drupal\veracity_vql\Event\VqlPreExecuteEvent;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Communicates with the Veracity API.
 */
class VeracityApi implements VeracityApiInterface {
  use StringTranslationTrait;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A shared Veracity Client for making API requets.
   *
   * @var \Drupal\veracity_vql\VeracityClient
   */
  protected $veracityClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new VeracityApi object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerInterface $logger, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->veracityClient = self::getVeracityClient($config_factory, $http_client);
    $this->logger = $logger;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection(string $endpoint, array $access_key): bool {
    $temp_client = new VeracityClient($this->httpClient, $endpoint, $access_key);
    $query = [
      'filter' => [
        '$limit' => 1,
      ],
      'process' => [],
    ];

    // This will throw if there is a problem.
    $result = $temp_client->analyze($query);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function executeVql(string $query): array {
    try {
      $pre_event = new VqlPreExecuteEvent($query);
      $this->eventDispatcher->dispatch($pre_event, VqlPreExecuteEvent::EVENT_NAME);

      $result = $this->veracityClient->analyze($pre_event->prepareQuery()) ?? [];

      $post_event = new VqlPostExecuteEvent($query, $result);
      $this->eventDispatcher->dispatch($post_event, VqlPostExecuteEvent::EVENT_NAME);
      return $post_event->getResult();
    }
    catch (\Exception $e) {
      $this->logger->warning('Encountered an error executing a VQL query: %error', ['%error' => $e->getMessage()]);
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint(): string {
    return $this->veracityClient->getEndpoint();
  }

  /**
   * {@inheritdoc}
   */
  public function getVqlRenderer(): string {
    $host = parse_url($this->veracityClient->getEndpoint(), PHP_URL_HOST);
    if (empty($host)) {
      throw new VeracityClientException($this->t("No LRS endpoint set. Please add a valid LRS endpoint to LRS configuration."));
    }
    return "https://$host/integrations/public/vqlUtils/renderer.js";
  }

  /**
   * Creates a Veracity client.
   */
  protected static function getVeracityClient(ConfigFactoryInterface $config_factory, ClientInterface $http_client): VeracityClient {
    ['endpoint' => $endpoint, 'access_key' => $access_key] = static::getVeracityConnectionConfig($config_factory);

    return new VeracityClient($http_client, $endpoint, $access_key);
  }

  /**
   * Determines the appropriate endpoint and credentials based on config.
   */
  protected static function getVeracityConnectionConfig(ConfigFactoryInterface $config_factory): array {
    // Check to see if there are integration-specific settings.
    $config = $config_factory->get('veracity_vql.settings');
    if (!empty($config->get('endpoint'))) {
      return [
        'endpoint' => $config->get('endpoint'),
        'access_key' => [
          $config->get('access_key_id'),
          $config->get('access_key_secret'),
        ],
      ];
    }
    return ['endpoint' => '', 'access_key' => []];
  }

}
